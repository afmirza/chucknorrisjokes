//
//  Networking.swift
//  ChuckNorrisJokes
//
//  Created by The App Experts on 27/06/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import Foundation

struct Jokes: Codable {
    var id: Int
    var joke: String
    var categories: [String]
}

struct Root: Codable {    
    var value: Jokes
}

struct Root2: Codable {
    var value: [Jokes]
}

//MARK:- URL Builder

struct URLBuilder {
    
    static func getJokeURL(numberOfJokes: Int, excludeExplicit: Bool) -> URLRequest?{
        
        var urlComps = URLComponents()
        
        urlComps.scheme = "http"
        urlComps.host = "api.icndb.com"
        urlComps.path = "/jokes/random/\(numberOfJokes)"
        
        if excludeExplicit{
            urlComps.queryItems = [
                URLQueryItem(name: "exclude", value: "[explicit]")
            ]
        }
        
        guard let url = urlComps.url else {return nil}
        print(url)
        return URLRequest(url: url)
    }
    
    static func getRandomJokeWithCustomCharacterURL(firstName: String, lastName:String, excludeExplicit: Bool) -> URLRequest?{
        
        var urlComps = URLComponents()
        
        urlComps.scheme = "http"
        urlComps.host = "api.icndb.com"
        urlComps.path = "/jokes/random/"

        if excludeExplicit {
            urlComps.queryItems = [
                URLQueryItem(name: "firstName", value: firstName),
                URLQueryItem(name: "lastName", value: lastName),
                URLQueryItem(name: "exclude", value: "[explicit]")
            ]
            
        }else {
            urlComps.queryItems = [
                URLQueryItem(name: "firstName", value: firstName),
                URLQueryItem(name: "lastName", value: lastName)
            ]
        }
        
        guard let url = urlComps.url else {return nil}
        print(url)
        return URLRequest(url: url)
    }

}

protocol NetworkSession {
    
    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
    
}

extension URLSession: NetworkSession{}

enum NetworkError: Error {
    case generalError(Error)
    case invalidResponseCode(Int)
    case noData
    case unknown
    case invalidURL
}

class Network{
    
    lazy var session: NetworkSession = URLSession.shared
    
    func load(request: URLRequest, completion: @escaping (Result<Data,NetworkError>) -> Void){
        let task = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(.failure(.generalError(error)))
            } else if let response = response as? HTTPURLResponse {
                switch response.statusCode {
                case 200...299:
                    if let data = data{
                        completion(.success(data))
                    }else{
                        completion(.failure(.noData))
                        
                    }
                default:
                    completion(.failure(.invalidResponseCode(response.statusCode)))
                }
            } else {
                completion(.failure(.unknown))
            }
        }
        task.resume()
    }
    
}

//MARK:- JSON Parsing

struct JSONParser {
    
    static func parse<T>(_ data: Data, into type: T.Type) -> T? where T: Codable {
        
        let decoder = JSONDecoder()
        
        do {
            return try decoder.decode(T.self, from: data)
        } catch {
            print(error)
            return nil
        }
        
    }
    
}
