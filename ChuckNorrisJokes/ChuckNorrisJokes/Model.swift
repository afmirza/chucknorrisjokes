//
//  Model.swift
//  ChuckNorrisJokes
//
//  Created by The App Experts on 27/06/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import Foundation

class Model {
    
    var network = Network()
    var jokes = [Jokes]()
    var excludeExplicit = false
    
    func getRandomJoke(completion: @escaping (Result<Root2, NetworkError>) ->Void){
        
        guard let randomJokeRequest = URLBuilder.getJokeURL(numberOfJokes: 1, excludeExplicit: excludeExplicit) else {
            completion(.failure(.invalidURL))
            return
        }
 
        
        network.load(request: randomJokeRequest) { (result) in
            switch result{
            case .success(let data):
                guard let response = JSONParser.parse(data, into: Root2.self) else{
                    completion(.failure(.unknown))
                    return
                }
                completion(.success(response))
                
            case .failure(let err):
                completion(.failure(err))
            }
        }
    }
    
    func getRandomJokeWithCustomCharacter(name:String, completion: @escaping (Result<Root, NetworkError>) ->Void){
        
        let fullName = name.components(separatedBy: " ")
        let firstName = fullName.first
        let lastName = fullName.last
        guard let randomJokeRequest = URLBuilder.getRandomJokeWithCustomCharacterURL(firstName: firstName!, lastName: lastName!, excludeExplicit: excludeExplicit) else {
            completion(.failure(.invalidURL))
            return
        }
        
        network.load(request: randomJokeRequest) { (result) in
            switch result{
            case .success(let data):
                guard let response = JSONParser.parse(data, into: Root.self) else{
                    completion(.failure(.unknown))
                    return
                }
                completion(.success(response))
                
            case .failure(let err):
                completion(.failure(err))
            }
        }
    }
    
    func getJokes( completion: @escaping () ->Void){
        
        guard let randomJokeRequest = URLBuilder.getJokeURL(numberOfJokes: 10, excludeExplicit: excludeExplicit) else {
            print("URL Error")
            return
        }
        
        network.load(request: randomJokeRequest) { (result) in
            switch result{
            case .success(let data):
                guard let response = JSONParser.parse(data, into: Root2.self) else{
                    print("Json decoding error")
                    return
                }
                self.jokes.append(contentsOf: response.value)
                completion()
                
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
}

extension Model{
    
    var sections: Int {
        return 1
    }
    func numberOfEntries(in section: Int) -> Int {
        return jokes.count
    }
    
    func item(at indexPath: IndexPath) -> Jokes? {
        if indexPath.section < 0 || indexPath.section > sections {
            return nil
        }
        
        if indexPath.row < 0 || indexPath.row > numberOfEntries(in: indexPath.section){return nil}
        
        return jokes[indexPath.row]
    }
}
