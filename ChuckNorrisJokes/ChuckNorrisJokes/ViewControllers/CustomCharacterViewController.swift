//
//  CustomCharacterViewController.swift
//  ChuckNorrisJokes
//
//  Created by The App Experts on 27/06/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class CustomCharacterViewController: UIViewController {
    
    @IBOutlet weak var textField: UITextField!
    var model: Model!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Custom Character"
        textField.layer.borderWidth = 2
        textField.placeholder = "Enter name for the custom character"
        print(model.excludeExplicit)
    }
    
    @IBAction func searchAction(_ sender: UIButton) {
        
        guard let name = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines), !name.isEmpty else{
            showAlert(title: "Error", message: "Please enter a name")
            return
        }
        
        model.getRandomJokeWithCustomCharacter(name: name) { (result) in
            switch result{
            case .success(let data):
                DispatchQueue.main.async {
                    
                    self.showAlert(title: "Joke", message: data.value.joke)
                }
            case .failure(let err):
                self.showAlert(title: "Error", message: err.localizedDescription)
            }
        }
    }
    
    func showAlert(title: String, message: String) {
        let alertView = UIAlertController(title: title,
                                          message: message,
            preferredStyle:. alert)
        let okAction = UIAlertAction(title: "Ok", style: .default)
        alertView.addAction(okAction)
        self.present(alertView, animated: true)
    }
}
