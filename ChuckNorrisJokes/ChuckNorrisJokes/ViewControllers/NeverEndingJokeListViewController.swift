//
//  NeverEndingJokeListViewController.swift
//  ChuckNorrisJokes
//
//  Created by The App Experts on 27/06/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class NeverEndingJokeListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var model: Model!
    var fetchingMore = false
    
    var visibleRows: [IndexPath]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        title = "Never Ending Joke List"
        
        model.getJokes() {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { context in
            // Save the visible row position
            self.visibleRows = self.tableView.indexPathsForVisibleRows
            context.viewController(forKey: UITransitionContextViewControllerKey.from)
        }, completion: { context in
            // Scroll to the saved position prior to screen rotate
            self.tableView.scrollToRow(at: self.visibleRows[0], at: .top, animated: false)
        })
    }
    
    func beginFetch() {
        fetchingMore = true
        model.getJokes() {
            // mimicing slow network, so that activity indicator can be seen while downloading more data
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0 , execute: {
                self.fetchingMore = false
                self.tableView.reloadData()
            })
        }
    }
}

//MARK:- Table View DataSource & Delegate

extension NeverEndingJokeListViewController: UITableViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.height{
            if !fetchingMore {
                beginFetch()
            }
        }
    }
    
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))

            self.tableView.tableFooterView = spinner
            self.tableView.tableFooterView?.isHidden = false
        }
    }
}

extension NeverEndingJokeListViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model.numberOfEntries(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.layer.borderWidth = 2.0
        cell.layer.cornerRadius = 10.0
        if let currentJoke = model.item(at: indexPath){
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.text = currentJoke.joke
        }
        
        return cell
    }
    
    
}
