//
//  ViewController.swift
//  ChuckNorrisJokes
//
//  Created by The App Experts on 27/06/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var model: Model!
    @IBOutlet weak var stateSwitch: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model = Model()
        stateSwitch.addTarget(self, action: #selector(toggleSwitch), for: .valueChanged)
    }

//MARK:- Actions
    @IBAction func fetchRandomJoke(_ sender: Any) {
        
        model.getRandomJoke() { (result) in
            switch result{
            case .success(let data):
                DispatchQueue.main.async {
                    guard let unwrapedJoke = data.value.first else {return}
                    self.showAlert(title: "Joke", message: unwrapedJoke.joke)
                }
            case .failure(let err):
                self.showAlert(title: "Error", message: err.localizedDescription)
            }
        }
    }
    
    @IBAction func textInputAction(_ sender: Any) {
        performSegue(withIdentifier: "customCharacter", sender: sender)
    }
    
    @IBAction func neverEndingListAction(_ sender: Any) {
        performSegue(withIdentifier: "neverEndingList", sender: sender)
    }
    
    @objc func toggleSwitch(){
        model.excludeExplicit.toggle()
    }
    
    func showAlert(title: String, message: String) {
        let alertView = UIAlertController(title: title,
                                          message: message,
                                          preferredStyle:. alert)
        let okAction = UIAlertAction(title: "Ok", style: .default)
        alertView.addAction(okAction)
        self.present(alertView, animated: true)
    }

//MARK:- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
            
        case "customCharacter":
            if let destinationViewController = segue.destination as?  CustomCharacterViewController {
                destinationViewController.model = model
            }
            
        case "neverEndingList":
            if let destinationViewController = segue.destination as?  NeverEndingJokeListViewController {
                destinationViewController.model = model
            }
        default:
            print("Segue w/ ID \(String(describing: segue.identifier)) not handled")
        }
        
        
    }

    
}

